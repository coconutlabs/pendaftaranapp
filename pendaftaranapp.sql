-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 22, 2016 at 01:37 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pendaftaranapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `tiket`
--

CREATE TABLE `tiket` (
  `id` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `kehadiran` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(5) NOT NULL,
  `a_nama` varchar(100) NOT NULL,
  `a_alamat` text,
  `a_email` varchar(100) NOT NULL,
  `a_password` varchar(255) NOT NULL,
  `a_no_tlp` varchar(17) NOT NULL,
  `a_kota_lahir` varchar(50) DEFAULT NULL,
  `a_tgl_bln_thn_lahir` date DEFAULT NULL,
  `a_jkelamin` enum('laki-laki','perempuan') DEFAULT NULL,
  `a_thnlulus` varchar(4) DEFAULT NULL,
  `a_asalsekolah` varchar(250) DEFAULT NULL,
  `a_alasanmasuk` text,
  `a_angkatan` int(3) DEFAULT NULL,
  `a_nra` varchar(11) DEFAULT NULL,
  `a_jabatan` text,
  `a_spesialisasi` text,
  `a_foto` varchar(100) DEFAULT NULL,
  `a_kode` varchar(255) NOT NULL,
  `level_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `a_nama`, `a_alamat`, `a_email`, `a_password`, `a_no_tlp`, `a_kota_lahir`, `a_tgl_bln_thn_lahir`, `a_jkelamin`, `a_thnlulus`, `a_asalsekolah`, `a_alasanmasuk`, `a_angkatan`, `a_nra`, `a_jabatan`, `a_spesialisasi`, `a_foto`, `a_kode`, `level_id`) VALUES
(1, 'Mannawari Syam', 'JL.Manggarupi No:79', 'Mannawarisyam@yahoo.co.id', 'e8180872d8e9f59c852b66136be1abcf', '085399092555', 'SUMIGO', '1992-02-02', 'laki-laki', NULL, NULL, NULL, 6, '06.14.002', 'Anggota Departemen Humas', '', '06.14.002.jpg', '5531b6bbc9fa27.22655132', 3),
(2, 'RM.Trian Ramadhan Diponegoro', 'BTN.Pao-pao Permai F4/9', 'trian.ramadhan92@gmail.com', 'e8180872d8e9f59c852b66136be1abcf', '085397661363', 'Ujung Pandang', '1992-05-14', 'laki-laki', NULL, NULL, NULL, 6, '06.14.012', 'Anggota Dept. Keorganisasian', 'Masih Dalam Pencarian....', '06.14.012.jpg', '5531b6bbc9fa27.22655132', 3),
(3, 'seno apriliyadi', 'jl.tamangapa raya 3 no 22c', 'seno.apriliyadi@gmail.com', 'e8180872d8e9f59c852b66136be1abcf', '089696679353', 'surabaya', '1991-04-11', 'laki-laki', '', '', '', 6, '06.14.001', 'wakil ketua umum', 'web programer', '3.jpg', '5531b6bbc9fa27.22655132', 3),
(4, 'Andy Abd. Azis', ' JL. DAENG TATA 3', 'andy.aziez@gmail.com', 'e8180872d8e9f59c852b66136be1abcf', '085394711024', 'Tana Toraja', '1995-06-22', 'laki-laki', NULL, NULL, NULL, 6, '06.14.009', 'anggota Dep. Keorganisasian', 'web programming', '06.14.009.jpg', '5531b6bbc9fa27.22655132', 3),
(5, 'suparman', 'A.P Pettarani 4', 'parman96@yahoo.com', 'e8180872d8e9f59c852b66136be1abcf', '085218707333', 'pasangkayu', '1995-07-13', 'laki-laki', NULL, NULL, NULL, 6, '06.14.008', 'Kordinator Keorganisasian', 'Multimedia', '06.14.008.jpg', '5531b6bbc9fa27.22655132', 3),
(6, 'NURWANA LUKMAN', 'JL. BONTOA JAYA NO.7 MAKASSAR', 'nurwanaluke01@gmail.com', 'e8180872d8e9f59c852b66136be1abcf', '085240240664', 'BULUKUMBA', '0000-00-00', 'perempuan', NULL, NULL, NULL, 6, '06.14.014', 'Anggota Depertemen Keorganisasian', 'program', '06.14.014.JPG', '5531b6bbc9fa27.22655132', 3),
(8, 'andre tupelu', 'komp. purn TNI AURI pai 3', 'andre.tupelu@gmail.com', 'e8180872d8e9f59c852b66136be1abcf', '082292437828', 'makassar', '1996-12-31', 'laki-laki', NULL, NULL, NULL, 6, '06.14.006', 'pembelajaran', 'multimedia', '06.014.006.jpg', '5531b6bbc9fa27.22655132', 3),
(9, 'Muh. Zulham U', 'Jl. Vetran Selatan Lr.2 Nm.6a', 'aam.zulham@gmai.com', 'e8180872d8e9f59c852b66136be1abcf', 'xxxxxxxxxxxxxx', 'makassar', '1993-06-10', 'laki-laki', NULL, NULL, NULL, 3, '03.11.012', 'DEPT. Penyiaran & Penerbitan', 'Design Grafis', '03.11.012.jpg', '5531b6bbc9fa27.22655132', 3),
(11, 'Hasrullah MS', 'JL.Samata, Villa Samata Sejahtera Blok A 24', 'beccebaco73@yahoo.com', 'e8180872d8e9f59c852b66136be1abcf', '089670478124', 'Ujung Pandang', '1995-04-24', 'laki-laki', NULL, NULL, NULL, 6, '06.14.010', 'Anggota Departemen Humas', 'Multimedia', '06.14.010.JPG', '5531b6bbc9fa27.22655132', 3),
(12, 'Abdul Rasyid Ramadhan', 'JL. Manuruki Raya', 'acitjelek@gmail.com', 'e8180872d8e9f59c852b66136be1abcf', '081944371506', 'Kendari', '1996-01-26', 'laki-laki', NULL, NULL, NULL, 6, '06.14.007', 'Koordinator Departemen HUMAS', 'Java Desktop', '06.14.007.jpg', '5531b6bbc9fa27.22655132', 3),
(13, 'A. Rizkiyanto Amir', 'BTN Pao Pao Block c2 Makassar', 't41k1t1@yahoo.com', 'e8180872d8e9f59c852b66136be1abcf', '081243733469', 'Makassar', '1988-05-12', 'laki-laki', NULL, NULL, NULL, 1, '01.09.006', 'koordinator departemen keorganisasian', 'Multimedia', '01.09.006.jpg', '5531b6bbc9fa27.22655132', 3),
(14, 'Yaomal', 'Perm. Pesona Mutiara Palangga Blok P2/11', 'yaomal.amd.kom@gmail.com', 'e8180872d8e9f59c852b66136be1abcf', '081319235799', 'ujung pandang', '1992-11-02', 'laki-laki', NULL, NULL, NULL, 2, '02.10.014', 'Kord Dept. Keorganisasian', 'Networkin', '02.010.014.jpg', '5531b6bbc9fa27.22655132', 3),
(15, 'samson pesireron', 'jl.A.Pettarani II lorong 2', 'pesireron.samson@yahoo.co.id', 'e8180872d8e9f59c852b66136be1abcf', '085240411823', 'Maluku (Ambon)', '1988-01-06', 'laki-laki', NULL, NULL, NULL, 5, '05.13.013', 'Angota', 'web', '05.13.013.JPG', '5531b6bbc9fa27.22655132', 3),
(17, 'Rusman', 'A.P.Pettarani 8', 'rhusman.05@gmail.com', 'e8180872d8e9f59c852b66136be1abcf', '085244826677', 'Nabire-papua', '1995-02-02', 'laki-laki', NULL, NULL, NULL, 5, '05.13.010', 'Koordinator Departemen penerbitan dan penyiaran', 'multimedia', '05.13.010.jpg', '5531b6bbc9fa27.22655132', 3),
(18, 'ahmad musawwir', 'jln.soekarno hatta lrn.3 no 3 benteng selayar', 'lubisawiee@gmail.com', 'e8180872d8e9f59c852b66136be1abcf', '085240779233', 'Benteng', '1992-09-08', 'laki-laki', NULL, NULL, NULL, 3, '03.11.009', 'Anggota', '', '03.11.009.jpg', '5531b6bbc9fa27.22655132', 3),
(19, 'M. Yunus Yusuf', 'Jl. Sukaria Raya No. 72 Makassar', 'kh3nz41@gmail.com', 'e8180872d8e9f59c852b66136be1abcf', '085299899953', 'Ujung Pandang', '1987-06-01', 'laki-laki', NULL, NULL, NULL, 1, '01.09.025', 'Anggota Departemen Pembelajaran', 'Programming Visual', '01.09.025.jpg', '5531b6bbc9fa27.22655132', 3),
(20, 'amanda sastra', 'jln. kakatua asmat brk 8/9', 'amandaboondtz@gmail.com', 'e8180872d8e9f59c852b66136be1abcf', '08991535040', 'makassar', '1992-05-08', 'perempuan', NULL, NULL, NULL, 5, '05.13.07', 'anggota keorganisasian', '', '05.13.07.jpg', '5531b6bbc9fa27.22655132', 3),
(21, 'ARMY SIGART ADMANI', 'Jl. Mangga II Pao-Pao. GOWA', 'armysigart@gmail.com', 'e8180872d8e9f59c852b66136be1abcf', '08114166006', 'UJUNG PANDANG', '1997-06-03', 'laki-laki', NULL, NULL, NULL, 4, '04.12.006', 'Anggota Departemen Keorganisasian (Periode 2012-2013)', 'Web Programer', '04.12.006.jpg', '5531b6bbc9fa27.22655132', 3),
(23, 'Suherman', 'Jl. Batua Raya, Makassar', 'suherman333@gmail.com', 'e8180872d8e9f59c852b66136be1abcf', '085299868221', 'Palopo', '1989-12-04', 'laki-laki', NULL, NULL, NULL, 3, '0311002', 'Ketua Umum', 'Program', '0311002.jpg', '5531b6bbc9fa27.22655132', 3),
(24, 'Muh Iqbal Halim', 'Jl. ABD Muthalib Dg. Narang No 209 Pao-Pao', 'iqbalexploit@yahoo.com', 'e8180872d8e9f59c852b66136be1abcf', '081355600080', 'Ujung Pandang', '1990-06-10', 'laki-laki', NULL, NULL, NULL, 1, '01.09.007', 'Anngota Departemen Organisasi', 'Networking', '01.09.007.JPG', '5531b6bbc9fa27.22655132', 3),
(25, 'rini tomasoa', 'Jln btn pepabri sudiang blok A1 no 9', 'tomasoa.rini@yahoo.com', 'e8180872d8e9f59c852b66136be1abcf', '082393235385', 'jakarta', '1995-07-11', 'perempuan', NULL, NULL, NULL, 7, '07.15.013', 'Anggota', '', '07.15.013.JPG', '5531b6bbc9fa27.22655132', 3),
(26, 'Arbi Oktoryan Benny', 'Jl.Syekh Yusuf BTN Tirta Sari Komp. PDAM', 'gojelryan@gmail.com', '52e02a732df70ffafd30f780fdd66499', '085200948081', 'Parepare', '1995-06-10', 'laki-laki', NULL, NULL, NULL, 7, '07.15.001', 'ANGGOTA', '', '07.15.001.jpg', '553348fc5355d2.27097173', 3),
(27, 'Eka Rahmawati', 'Bontorita, Desa Bontomangape, Kec. Galesong, Kab. Takalar', 'ekarahmawati615@gmal.com', 'e8180872d8e9f59c852b66136be1abcf', '082396444194', 'Takalar', '1994-12-04', 'perempuan', NULL, NULL, NULL, 5, '0513015', 'Departemen Pembelajaran', 'Pemrograman', '0513015.jpg', '5531b6bbc9fa27.22655132', 3),
(28, 'Ratnawati', 'Jl. A.P Pettarani No.24 Makassar', 'ratnawatytasriqtaslim@yahoo.com', 'e8180872d8e9f59c852b66136be1abcf', '085145404302', 'Takalar', '1997-05-07', 'perempuan', NULL, NULL, NULL, 7, '07.15.012', 'ANGGOTA', '', '07.15.012.jpg', '5531b6bbc9fa27.22655132', 3),
(29, 'Andika', 'Jl. Hertasning baru komp, permata hijau lestari p16/01', 'andikap32@gmail.com', 'e8180872d8e9f59c852b66136be1abcf', '085255319190', 'sukaraja', '1990-07-07', 'laki-laki', NULL, NULL, NULL, 3, '0311013', 'Humas', 'Pemograman', '0311013.jpg', '5531b6bbc9fa27.22655132', 3),
(30, 'Yos Arasandi Mangentang', 'jl.telepon 6, telkomas , makassar', 'oliveryosi@gmail.com', 'e8180872d8e9f59c852b66136be1abcf', '081294696015', 'Jakarta', '2016-06-04', 'laki-laki', NULL, NULL, NULL, 6, '06.14.011', 'Anggota Departemen Pembelajaran', 'C Programing, Linux Programing', '06.14.011.jpg', '5531b6bbc9fa27.22655132', 3),
(32, 'M.BUDIMAN', 'JL.ANDI DEWANG', 'budi071188@yahoo.com', '0be3cc6b27b214fde6c93d11fdf654f7', '081245982223', 'parepare', '1988-07-11', 'laki-laki', NULL, NULL, NULL, 3, '001', 'kesekretariat', 'linux', '001.JPG', '5531f406e36869.02853127', 3),
(33, 'Ilham Basalama', 'BTN AURA PERMAI BLOK D3 No. 23', 'ilhambas02@gmail.com', '7f25222e2358c4c684bc82ed75f8db23', '081241030201', 'Makassar', '1991-10-01', 'laki-laki', NULL, NULL, NULL, 1, '01.09.012', 'Dept Organisasi', 'Multimedia', '01.07.012.JPG', '5532c6bf355750.22121266', 3),
(34, 'Hilman', 'Jl.Flamboyan No.23B', 'hilmananugrah@gmail.com', '4ad3b0340849b984dfd60dd68470c154', '085299625662', 'Ujung Pandang', '1993-03-07', 'laki-laki', NULL, NULL, NULL, 5, '05.13.004', 'Koord. Dept. Pembelajaran', 'Programming', '05.13.004.jpg', '553344305bc041.07953564', 3),
(35, 'Hasra Mi''raj Ibrahim', 'Jl.Pettarani 5', 'Hasra.Mijrat@gmail.com', 'afd9e4aef3ca4c31cc7a75fa7d0dccdc', '089674560257', 'Ujung Pandang', '1995-01-09', 'perempuan', NULL, NULL, NULL, 7, '07.15.004', 'Anggota', 'dalam pencarian\r\n', '07.15.004.jpg', '5545f248c9ed03.23613129', 3),
(36, 'Muhammad Asri', 'jalan A.P Petttarani 2 lrg 12 nomer 8.Makassar', 'muhammad.asri.ali@gmail.com', 'ac7bf710112ffc227bf856a21af6bd25', '085390999840', 'Temerloh Malaysia', '1987-10-02', 'laki-laki', NULL, NULL, NULL, 5, '05.13.008', 'Ketua Umum', 'program', '05.13.008.JPG', '5540794ef31b83.66305371', 3),
(37, 'iqbal nasikin ', 'Jl. Mampang Prapatan I No.37 B\r\nRT. 007 RW. 001 Mampang Prapatan\r\nMampang Prapatan, Jakarta Selatan 12790', 'iqbal.nasikin@gmail.com', '63defe2640e4b71a727ed0b2e1d1ce68', '085242627688', 'jakarta selatan', '1999-09-09', 'laki-laki', NULL, NULL, NULL, 0, '00.01.011', 'executor', 'win xp os', '00.01.011.jpg', '55524b15591109.39007556', 3),
(38, 'elfira febrianti.k', 'nusantara baru no 408', 'elfirafebrianti23@yahoo.com', 'bb78f494935736aa1c0abb242f3a07b1', '089647191181', 'makassar', '1995-05-30', 'perempuan', NULL, NULL, NULL, 6, '0614003', 'ketua umum', 'web', '0614003.JPG', '55547b25418999.62884252', 3),
(39, 'jumhaerani', 'jln.nuri 12 L sungguminasa', 'ranisajalah@gmail.com', '97ebd58ef01cde8a6aed6bc26c20dda2', '089639848737', 'bulukkumba', '1997-12-31', 'perempuan', NULL, NULL, NULL, 7, '07.15.019', 'anggota', '', '07.15.019.jpg', '5555be1940ddb8.59107457', 3),
(40, 'Adi Permadi', 'Aspol Panaikang. Blok C. Nmr, 44', 'adhy.lkc73@gmail.com', '66dedbe8d96e4b12bbb7942a6ec66d8a', '082291266469', 'POLEWALI MANDAR', '1994-08-09', 'laki-laki', NULL, NULL, NULL, 7, '07.15.005', 'Anggota', 'Jaringan Komputer', '07.15.005.jpg', '5555be04ca06e9.29707394', 3),
(41, 'Bagas eryan bimantoro', 'btn bumi samata permai blok c2. no.23', 'bagaseryanto@gmail.com', '8d74033588e2341c2a6068c1651a5188', '081355235829', 'makassar', '1996-06-19', 'laki-laki', NULL, NULL, NULL, 7, '07.15.017', 'anggota', 'pemrograman', '07.15.017.JPG', '5555c076f2f7b9.31125786', 3),
(42, 'A.Amatul firdalisyah', 'Btn bontomajannang kab.gowa', 'aamatulfirdalisyah@gmail.com', 'f091ee436c4bb778c42b7f7b2e9541f4', '089697706907', 'makassar', '1997-08-22', 'perempuan', NULL, NULL, NULL, 7, '07.15.010', 'Anggota', '', '07.15.010.jpg', '5555cf10e19533.38955340', 3),
(43, 'Muh.Akbar', 'Jl.Mentimun no.12b', 'akbar191194@gmail.com', '6ca0219924256eb7ddb661386dea3645', '089639995364', 'Ujung Pandang', '1994-11-19', 'laki-laki', NULL, NULL, NULL, 7, '0715014', 'Anggota', '', '0715014.jpg', '555605aaab9445.42142656', 3),
(44, 'Riskha', 'Jl.A. P.Pettarani IV', 'riskhapratiwi777@gmail.com', '3d419771eba9bf5fa33f05e5703fbd9c', '085146044175', 'maros', '1996-10-11', 'perempuan', NULL, NULL, NULL, 7, '0715008', 'anggota', 'belum ada', '0715008.jpg', '55560b40330863.21269584', 3),
(45, 'Elifas Agus Saldy', 'Jln.BUKIT BATU lr.8 no.6, Kel.Antang, Kec.Manggala', 'elifas_agussaldy@yahoo.co.id', 'c42713a59487ec9237465263a3ea015f', '085242249005', 'Makassar', '1996-07-06', 'laki-laki', NULL, NULL, NULL, 7, '07.15.009', 'Anggota', 'Jaringan', '07.15.009.JPG', '5556be667aaf90.74533717', 3),
(46, 'Arhamul Fadillah', 'BTP Blok AC1 No.48', 'arhamfadill@gmail.com', '99d134c8cb872ec9ff3349695bce148e', '085399933039', 'Bulukumba', '1996-08-07', 'laki-laki', NULL, NULL, NULL, 7, '07.15.011', 'Anggota', 'Networking', '07.15.011.JPG', '5559b43bd570d5.05747237', 3),
(47, 'TRI NIRWANA', 'JL. KESATUAN 1 NO.6B MAKASSAR', 'biyuchatrhie@yahoo.com', '45b2b1415e228389b012ebb16216fd8f', '085299887070', 'UJUNG PANDANG', '1992-11-21', 'perempuan', NULL, NULL, NULL, 7, '07.15.006', 'ANGGOTA', 'Searching....', '07.15.006.jpg', '55df264c129e93.54186748', 3),
(50, 'sriwati basri', 'BORONG BAJI KEC.POLUT KAB.TAKALAR', 'sriwatibasri97@gmail.com', '3aa69517fd17bf708a35ff1f097a2adf', '082348669792', 'TAKALAR', '1998-03-08', 'perempuan', NULL, NULL, NULL, 7, '07.15.015', 'ANGGOTA', 'WEB', '07.15.015.jpg', '556fc2e1f0bdb8.45752537', 3),
(51, 'Reymon christo laga', 'Jalan kakaktua asrama mattoanging barak 8 nomor 1', 'reymonchristolaga@gmail.com', '3134dc835be7a13a60239aa683afd6d7', '081343866636', 'Makassar', '1984-05-08', 'laki-laki', NULL, NULL, NULL, 4, '04.012.005', 'Koordinator Departemen penerbitan dan penyiaran', 'WEB', '04.012.005.jpg', '556fc49dccfb00.94016459', 3),
(52, 'Andi Tenroaji Ahmad', 'Jln. Ratulangi Lr.5', 'andi_tenroaji@yahoo.co.id', 'dd3575ad3bc1e84af5987d4418abb67f', '089515409303', 'makassar', '1997-08-01', 'laki-laki', NULL, NULL, NULL, 6, '14.06.005', 'Kordinator Pembelajaran', 'Program', '14.06.005.jpg', '558285d089dd12.55636399', 3),
(53, 'Andi Melinda Mattalatta', 'Jl. Sunu Komplex UNHAS, Blok PX 9B, Makassar, SULSEL', 'amel2352@gmail.com', '49d80bb42985c2e83836c0d18336be50', '082340806610', 'Bone', '1992-05-08', 'perempuan', NULL, NULL, NULL, 3, '0307013', 'Bendahara', 'Pemrograman', '0307013.jpg', '55887e41e58a19.99025942', 3),
(54, 'Ahdy Atma', 'btn.bakolu blok.A 11 No.12 Gowa,Makassar', 'ahdy.atma@gmail.com', '80114584651b6ae501d49e42e53e24c7', '082310294898', 'Dili', '1988-10-11', 'laki-laki', NULL, NULL, NULL, 1, '01.09.005', 'Wakil Ketua', 'Programming \r\nNetworking \r\nDesain', '01.01.05.jpg', '55a0e547ef0574.21601398', 3),
(55, 'Farah Inas Fairuz', 'JL. Muh. Jufri 3 No. 21 Makassar', 'farahinasfairuz@gmail.com', '0a966794cec6cc06759bad03ce6b8231', '089665014621', 'Makassar', '1998-12-05', 'perempuan', NULL, NULL, NULL, 7, '07.15.003', 'Anggota', 'Multimedia', '07.15.003.jpg', '55cdf6de4d2946.85681310', 3),
(56, 'Suryadi', 'Komp. Hasanuddin D40', 'sokasuryadi@gmail.com', '5c8eae8977eb639793c9c2271913380b', '087840633446', 'Selayar', '1998-02-03', 'laki-laki', NULL, NULL, NULL, 7, '002', 'Anggota', 'Belum ada', '002.jpg', '55ce1e0f4f5090.65921531', 3),
(57, 'P A C H R I', 'I n d o n e s i a', 'fachrimanutd@hotmail.com', '55e38ebbb48111bdbc25296db0494a83', '+6281242844572', 'Majene', '1985-02-05', 'laki-laki', NULL, NULL, NULL, 0, '00.00.004', 'Koordinator Pembelajaran', '', '00.00.004.jpg', '55e648b9ea51d9.47389142', 3),
(58, 'Istifarni Yandri', 'BTN. Ranggong Permai Blok.A No. 16', 'iceteaicecream@gmail.com', '52b2f03f3721fe8f9772f29d4e5c42e7', '085255665569', 'Makassar', '1993-04-01', 'perempuan', NULL, NULL, NULL, 2, '02.10.016', 'Anggota Dept. Humas Internal', 'Multimedia', '02.10.016.jpg', '55e7d83f1abbb4.24763437', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tiket`
--
ALTER TABLE `tiket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tiket`
--
ALTER TABLE `tiket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
