<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <script src="https://use.fontawesome.com/d179b46c0e.js"></script>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Daftar | Coconut.or.id',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Beranda', 'url' => ['/site/index']],
            ['label' => 'Tentang', 'url' => ['/site/about']],
        //  ['label' => 'Kontak', 'url' => ['/site/contact']],
            ['label' => 'Anggota', 'url' => ['/users/index']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Masuk', 'url' => ['/site/login']]
            ) : (
           
             '<li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="'.Yii::$app->request->baseUrl . '/uploads/users/' .Yii::$app->user->identity->a_foto.'" class="img-circle" width="30px" height="30px" alt="User Image">
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="dropdown-header">
               <center> <img src="'.Yii::$app->request->baseUrl . '/uploads/users/' .Yii::$app->user->identity->a_foto.'" class="img-circle" width="110px" height="110px" alt="User Image"> </center>
                <p>
              '.Yii::$app->user->identity->a_nama.'
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
               <p class="text-right">'.Html::a("Profil", Url::toRoute(["users/view", "id" => Yii::$app->user->identity->id]),["class"=>"btn btn-primary"]).' '.
                    Html::a("Keluar", Url::toRoute(["site/logout"]),["class"=>"btn btn-primary"]).'&nbsp;</p>
                <!-- /.row -->
              </li>'
                                        
            ),
        ],
    ]);
    NavBar::end();
    ?>
    
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </div>    
        <?= $content ?>
    
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
