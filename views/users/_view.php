<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;
?>
<div class="post col-md-3">
    <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title"><?= Html::encode($model->a_nama) ?></h3>
        </div>
        <div class="panel-body">
            <?php if($model->a_foto!=null){
                echo Html::img('@web/uploads/users/'.$model->a_foto,['width'=>'150','height'=>'150','class'=>'img-circle']);
            }else{
                echo Html::img('@web/uploads/konten/l.png',['width'=>'150','height'=>'150','class'=>'img-circle']);
            } ?>
            <?= Html::a('Profil', Url::toRoute(['users/view', 'id' => $model->id]),['class'=>'btn btn-success','title' => $model->a_nama]) ?>
        </div>
    </div>    
</div>
