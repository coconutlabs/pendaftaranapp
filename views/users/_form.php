<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\captcha\Captcha;
/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>

    <h2>Profil Umum</h2>
    
    <?= $form->field($model, 'a_nama')->textInput(['maxlength' => true,'disabled'=>true]) ?>

    <?= $form->field($model, 'a_alamat')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'a_email')->textInput(['maxlength' => true,'disabled'=>true]) ?>

    <?= $form->field($model, 'a_password')->passwordInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'a_no_tlp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'a_kota_lahir')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'a_tgl_bln_thn_lahir')->textInput() ?>

    <?= $form->field($model, 'a_jkelamin')->dropDownList([ 'laki-laki' => 'Laki-laki', 'perempuan' => 'Perempuan', ], ['prompt' => '']) ?>
    <h2>Profil Pendidikan</h2>
    <?= $form->field($model, 'a_thnlulus')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'a_asalsekolah')->textInput(['maxlength' => true]) ?>
    <h2>Profil Keanggotaan</h2>
    <?= $form->field($model, 'a_alasanmasuk')->textarea(['rows' => 6]) ?>
    
    <?php if($model->level_id <= 3){?>
    
    <?= $form->field($model, 'a_angkatan')->textInput() ?>

    <?= $form->field($model, 'a_nra')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'a_jabatan')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'a_spesialisasi')->textarea(['rows' => 6]) ?>
    
    <?php } ?>

    <?= $form->field($model, 'file')->fileInput() ?>

     <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
        'template' => '{image}{input}',
    ]) ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
