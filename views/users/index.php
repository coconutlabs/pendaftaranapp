<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">
    <div class="container">
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <?php 
        echo "<li role='presentation' class='active'><a href='#' aria-controls='home' role='tab' data-toggle='tab'><span class='glyphicon glyphicon-user'></span></a></li>";      
          
        foreach ($angka as $aa){
            if ($aa->a_angkatan == 0){
          echo "<li role='presentation'><a href='#angkatan$aa->a_angkatan' aria-controls='angkatan$aa->a_angkatan' role='tab' data-toggle='tab'>Badan Pendiri</a></li>";      
            }else{
          echo "<li role='presentation'><a href='#angkatan$aa->a_angkatan' aria-controls='angkatan$aa->a_angkatan' role='tab' data-toggle='tab'>Agk $aa->a_angkatan</a></li>";  
            }
        }
    ?>
  </ul>
  <div class="tab-content">
        <?php 
            foreach ($angka as $a){
             echo "<div role='tabpanel' class='tab-pane active' id='angkatan$a->a_angkatan'>";
              if ($a->a_angkatan == 0){
                       echo "<h2> Badan Pendiri</h2>";
                 }else{
                       echo "<h2> Angkatan ".$a->a_angkatan."</h2>";
                  }
            
             echo "<div class='row'>";
             echo  ListView::widget([
                'dataProvider' => $dataProvider[$a->a_angkatan],
                'itemView' => '_view',
                'layout'=>'{items}'

            ]); 
             echo "</div></div>"; 
            }
        ?>
  </div>

</div>
      
    
