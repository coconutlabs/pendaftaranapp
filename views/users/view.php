<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = $model->a_nama;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-view">
  <div class="container">  
      <div class="row">
        <div class="col-lg-3">
            <center> 
                <?= Html::img('@web/uploads/users/'.$model->a_foto,['class'=>'img-responsive img-thumbnail']) ?>
            </center>      
        </div>
        <div class="col-lg-8">
            <?php if(Yii::$app->user->identity->level_id==3){?>
            <h2>Profil Umum</h2>
          <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'a_nama',
            'a_alamat:ntext',
            'a_email:email',
            'a_no_tlp',
            'a_kota_lahir',
            'a_tgl_bln_thn_lahir',
            'a_jkelamin',
        ],
    ]) ?>  
             <h2>Profil Pendidikan</h2>
             <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'a_thnlulus',
            'a_asalsekolah',
        ],
    ]) ?>  
              <h2>Profil Keanggotaan</h2>
             <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'a_alasanmasuk:ntext',
            'a_angkatan',
            'a_nra',
            'a_jabatan:ntext',
            'a_spesialisasi:ntext',
        ],
    ]) ?>
            <?php }else{ ?>
            
              <div class="alert alert-info">
                  <h2 class="text-danger">Profil <small> <?= $model->a_nama ?> </small></h2>
                  <p class="text-danger">
                      Maaf anda belum diperbolehkan melihat profil lengkap saudara/i <?= $model->a_nama ?>
                  </p><br />
                  <?= Html::a('Kembali', Url::toRoute(['users/index']),['class'=>'btn btn-lg btn-danger']) ?>
              </div>
              
            <?php } ?>
        </div>
    </div>
  </div>
</div>
