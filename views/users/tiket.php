<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Users */

?>
<div class="users-view">
     
  <div class="container"> 
      
      <h3 class="text-info alert alert-danger">WORKSHOP | COCONUT</h3>
      <table width="100%">
          <tr>
              <td width="65%">
                  <span style="font-size: 30px;">Membangun</span> 
                  <span class="alert-success" style="font-size: 50px">Aplikasi</span> <br />
                  <span class="alert-info" style="font-size: 40px">Database</span>
                  <span style="font-size: 20px"> Menggunakan </span>
                  <span class="alert-warning" style="font-size: 50px">PHP</span>
              </td>
              <td>
                  <table>
                      <tr class="bg-info">
                         <td valign="top">
                                No  
                          </td>
                          <td valign="top">
                                : 
                          </td>
                          <td valign="top">
                              <b>    <?= ' ON'.$model->id ?> </b>
                          </td>
                      </tr>  
                       <tr>
                        <td valign="top">
                              Tempat
                        </td>
                        <td valign="top">
                              : 
                        </td>
                        <td valign="top">
                            <b>  Meeting Room <br /> STMIK PROFESIONAL MAKASSAR </b>
                        </td>
                    </tr>
                    <tr class="bg-info">
                        <td valign="top">
                              Tgl/waktu
                        </td>
                        <td valign="top">
                              : 
                        </td>
                        <td valign="top">
                            <b>  18 Juni 2016,<br /> 10.00 - Selesai </b>
                        </td>
                    </tr>
                             </table>            
              
              </td>
             
         
      </table>    
      <h5 class="alert alert-success">Contact Person : Tri Nirwana - 0852 9988 7070 / Faisal : 0896 9728 6052 </h5>
       
                

  </div>
</div>
