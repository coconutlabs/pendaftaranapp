<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'Update Users: ' . $model->a_nama;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="users-update">
    <div class="container">
         <div class="row">
            <div class="col-lg-3">
                <center> 
                    <?= Html::img('@web/uploads/users/'.$model->a_foto,['class'=>'img-responsive img-thumbnail']) ?>
                </center> 
            </div>
            <div class="col-lg-8">
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>
            </div>
         </div>
    </div>
</div>
