<?php

/* @var $this yii\web\View */

use yii\helpers\Html;


$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
   <div class="container">  
     <div class="row">
        <div class="col-md-12">
        <center>
            <?php echo Html::img('@web/uploads/konten/coconut.png',['width'=>'150','height'=>'190']) ?>
        </center>
        </div>
    </div>
    <div class="row">
        <div class="col-md-1">
            <span class="fa fa-5x fa-rocket text-warning"></span>
        </div>
        <div class="col-md-6">
            <h1><b>Tujuan <small>COCONUT</small></b></h1>
            <p>
                <b>COCONUT</b> bertujuan membentuk mahasiswa yang kreatif untuk mewujudkan Tri
    Dharma Perguruan Tinggi dan Sumpah Mahasiswa yang mengarah pada 
    pengembangan kreativitas mahasiswa dalam bidang penalaran dan keilmuan yang
    berkaitan dengan spesialisasi  IT.
            </p>
        </div>
        
    </div>
    <div class="row">
        <div class="col-md-1 col-md-offset-5">
            <span class="fa fa-5x fa-cogs text-info"></span>
        </div>
        <div class="col-md-6 ">
            <h1><b>Usaha <small>COCONUT</small></b></h1>
            <p>
            <ul>
               <li>
                Mengarahkan Mahasiswa untuk  berperan aktif dalam mewujudkan cita – cita 
                dan tujuan nasional  melalui Tri Dharma perguruan tinggi dan Sumpah
                Mahasiswa. 
               </li>
               <li>
                Membina dan mengembangkan kualitas serta meningkatkan kesejahteraan
                anggota 
               </li>
               <li>
                Usaha-usaha lain secara nyata dan bertanggung jawab yang menunjang tujuan
                organisasi.
               </li>
            </ul>
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-1">
            <span class="fa fa-5x fa-users text-success"></span>
        </div>
        <div class="col-md-6">
            <h1><b>Status <small>COCONUT</small></b></h1>
            <p>
                <b>COCONUT</b> adalah organisasi kemahasiswaan yang berbentuk media pembelajaran
                sosial dan sains yang bersifat otonom 
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-md-offset-6">

        </div>
    </div>

<hr class="alert-info">
   </div>
</div>
