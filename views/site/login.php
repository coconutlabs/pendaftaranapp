<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <div class="container">   
        <div class="row">
            <div class="col-lg-7">

            </div>
            <div class="col-lg-4">
                <blockquote>
                 <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to login:</p>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "<div class=\"col-lg-12\">{input}{error}</div>\n",
           
        ],
    ]); ?>

        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'password')->passwordInput() ?>

        <?= $form->field($model, 'rememberMe')->checkbox([
            'template' => "<div class=\"col-lg-12\">{input} {label}{error}</div>\n",
        ]) ?>
       
            <?= Html::submitButton('Masuk', ['class' => 'btn btn-primary btn-lg', 'name' => 'login-button']) ?>
            
      

    <?php ActiveForm::end(); ?>
                </blockquote>
            </div>
        </div>
   
  </div>  
</div>
