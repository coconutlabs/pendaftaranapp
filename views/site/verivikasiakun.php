<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;

$this->title = 'Verivikasi Akun';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-verivikasiakun">
    <div class="container">   
        <center> 
            <?php if (Yii::$app->session->hasFlash('kodesend')): ?>
        <div class="alert alert-success">
            Kode Verivikasi anda telah dikirim ke email anda.
        </div>
            <?php endif; ?>
                 <h1><?= Html::encode($this->title) ?></h1>
                 <p>Silahkan Memasukkan Kode Verivikasi yang telah dikirim di email kamu dan ketik kode captcha
                 <br />Jika Belum Menerima Kode Verivikasi klik <?= Html::a('Kirim Ulang Kode Verivikasi', Url::toRoute(['site/kirimkode'])) ?>
                 </p>
                 
    <?php $form = ActiveForm::begin([
        'id' => 'verivikasiakun-form',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "<div class=\"col-md-2 col-md-offset-5\">{input}{error}</div>\n",
           
        ],
    ]); ?>

        <?= $form->field($model, 'kodeverivikasi')->textInput(['autofocus' => true]) ?>
                 
        <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
           'template' => '{image}{input}',
       ]) ?>
        <?= Html::submitButton('Verivikasi Akun Saya', ['class' => 'btn btn-primary btn-lg', 'name' => 'login-button']) ?>
            
      

    <?php ActiveForm::end(); ?>
        </center> 
            </div>
        </div>
 
