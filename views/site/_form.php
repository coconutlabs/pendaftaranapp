<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>
<h3 class="alert alert-danger"><span class="fa fa-pencil text-info"></span> Ayo Belajar Bersama <small>Coconut</small></h3>
<div class="users-form">

    <?php $form = ActiveForm::begin([
        'id' => 'users-form',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "<div class=\"col-sm-12\">{input}{error}</div>\n",
        ],
    ]); ?>

    <?= $form->field($model, 'a_nama')->textInput(['maxlength' => true , 'placeholder'=>'Nama Lengkap'])?>

    <?= $form->field($model, 'a_email')->Input('email',['maxlength' => true , 'placeholder'=>'E-Mail'])?>
    
    <?= $form->field($model, 'a_no_tlp')->textInput(['maxlength' => true, 'placeholder'=>'Nomor Telepon'])?>

    <?= $form->field($model, 'a_password')->passwordInput(['maxlength' => true, 'placeholder'=>'Password Akun'])?>

    <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
        'template' => "<div class=\"col-sm-6\">{image}</div><div class=\"col-sm-6\">{input}</div>\n"
    ]) ?>
  
        <?= Html::submitButton($model->isNewRecord ? 'Registrasi' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn-lg btn-block' : 'btn btn-primary']) ?>


    <?php ActiveForm::end(); ?>

</div>
