<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;
$foto = Yii::$app->user->identity->a_foto;
?>
<h2>Halo ! <br /> <small><?=  Yii::$app->user->identity->a_nama ?></small></h2>
<center>
    <?php if($foto!=null){
        echo Html::img('@web/uploads/users/'.$foto,['width'=>'150','height'=>'150','class'=>'img-circle']);
    }else{
        echo Html::img('@web/uploads/konten/l.png',['width'=>'150','height'=>'150','class'=>'img-circle']);
    }
    ?>
<br /><br />
<?= Html::a('Profil Saya', Url::toRoute(['users/view', 'id' => Yii::$app->user->identity->id]),['class'=>'btn btn-primary']) ?>
</center>      
