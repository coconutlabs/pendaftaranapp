<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Users;
use yii\web\NotFoundHttpException;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout','verivikasiakun'],
                'rules' => [
                    [
                        'actions' => ['logout','verivikasiakun'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
   /*         'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
           ],
    * 
    */
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
          $model = new Users();

        if ($model->load(Yii::$app->request->post())) {
            
            if (($m = Users::findOne(['a_email'=>$model->a_email])) !== null) {
             throw new NotFoundHttpException('Email Sudah Dipakai Silahkan Login atau Daftar dengan email lain');
            }
            
            $model->a_kode=$model->generateSalt(); 
            $model->a_password=$model->hashPassword($model->a_password,$model->a_kode);
            $model->level_id = 5;
            if ($model->save()){
                $kode = substr($model->a_kode, 0, 6);
                if($this->sendkode($model->a_email, $kode)){
                    return $this->redirect(['users/view', 'id' => $model->id]);
                }
            }
        } else {
            return $this->render('index', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionVerivikasiakun()
    {
       
        $iduser = Yii::$app->user->identity->id;
        $model= Users::findOne($iduser);
        $akode = substr($model->a_kode, 0, 6);
         if ($model->load(Yii::$app->request->post())) {
            if ($akode == $model->kodeverivikasi){
                $model->level_id = 4;
             
            if ($model->save()){
                return $this->redirect(['users/view', 'id' => $model->id]);
            }
            } 
         }
         
        return $this->render('verivikasiakun', [
            'model' => $model,
        ]);
    }
    
   public function sendkode($email,$kode){
                $message=Yii::$app->mailer->compose();
                $message->setFrom('mailer@coconut.or.id');
                $message->setTo($email);
                $message->setSubject('Kode Verivikasi COCONUT' );
                $message->setHtmlBody('Kode verivikasi akun anda adalah <h2>'.$kode.'</h2> Silahkan verivikasi di daftar.coconut.or.id'
                        . 'atau dengan mengkilk link berikut'); 
              if ($message->send()){
              return true;
              }
   }
      
   public function actionKirimkode()
    {
        $iduser = Yii::$app->user->identity->id;
        $model= Users::findOne($iduser);
        $akode = substr($model->a_kode, 0, 6);
        if($this->sendkode($model->a_email, $akode)) {
            Yii::$app->session->setFlash('kodesend');

            return $this->redirect(['verivikasiakun']);
        }else{
          Yii::$app->session->setFlash('kode gagal terkirim');

            return $this->redirect(['verivikasiakun']);
        }
    } 

}
