<?php

namespace app\controllers;

use Yii;
use app\models\Users;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use kartik\mpdf\Pdf;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','view','update','create','delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index','view','update'],
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        // mengambil data angkatan
        $jumlah_angkatan = Users::findBySql('SELECT DISTINCT a_angkatan from users where level_id = 3 ORDER BY a_angkatan ASC')->all();
        foreach ($jumlah_angkatan as $a){
        $dataProvider[] = new ActiveDataProvider([
            'query' => Users::findBySql('SELECT * from users WHERE a_angkatan = '.$a->a_angkatan.' ORDER BY a_nama ASC'),
        ]);
    }
        
        
        return $this->render('index', [
            'dataProvider' => $dataProvider,'angka'=>$jumlah_angkatan,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        
        if(Yii::$app->user->identity->id == $id){
            if(Yii::$app->user->identity->level_id<=4){
            return $this->render('profil', [
              'model' => $model,
            ]);}
            else{
               return $this->redirect(['site/verivikasiakun']); 
            }
        }else{
            return $this->render('view', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Users();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $pas = $model->a_password;
        
        if ($model->load(Yii::$app->request->post())) {
         
            //cek penggantian password
         if($pas != $model->a_password){
            $model->a_kode=$model->generateSalt(); 
            $model->a_password=$model->hashPassword($model->a_password,$model->a_kode);
         }   
         
           //cek penggantian foto
         if(strlen(trim(UploadedFile::getInstance($model,'file')))>0){
             $model->file = UploadedFile::getInstance($model,'file');
             $foto = $model->a_foto;
             $model->a_foto = $model->id.'.'.$model->file->extension;
             $fg = 2;
         }else{
             $fg = -1;
             
         }
         
         if($model->save()){   
             //upload foto
             if($fg > 0){ 
                @unlink('uploads/users/'.$foto);
                $model->file->saveAS('uploads/users/'.$model->a_foto); 
             }
             
            return $this->redirect(['view', 'id' => $model->id]);
            }           
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionReport() {
    // get your HTML raw content without any layouts or scripts
    $model = $this->findModel(Yii::$app->user->identity->id);
    $content = $this->renderPartial('tiket',['model'=>$model]);
 
    // setup kartik\mpdf\Pdf component
    $pdf = new Pdf([
        // set to use core fonts only
        'mode' => Pdf::MODE_UTF8, 
        // A4 paper format
        'format' => Pdf::FORMAT_A4, 
        // portrait orientation
        'orientation' => Pdf::ORIENT_PORTRAIT, 
        // stream to browser inline
        'destination' => Pdf::DEST_BROWSER, 
        // your html content input
        'content' => $content,  
        // format content from your own css file if needed or use the
        // enhanced bootstrap css built by Krajee for mPDF formatting 
        //'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
         'cssFile' => '@vendor/bower/bootstrap/dist/css/bootstrap.min.css',
        // any css to be embedded if required
        'cssInline' => '.kv-heading-1{font-size:18px}', 
         // set mPDF properties on the fly
        'options' => ['title' => 'Workshop | COCONUT'],
         // call mPDF methods on the fly
     //   'methods' => [ 
     //       'SetHeader'=>['Workshop | COCONUT'], 
     //       'SetFooter'=>['{PAGENO}'],
     //   ]
        
    ]);
 
    // return the pdf output as per the destination setting
    return $pdf->render(); 
}
}
