<?php

namespace app\models;

use Yii;
use yii\base\Model;

class Verivikasiakun extends model
{
    public $kodeverivikasi;
     public function rules()
    {
        return [
            // username and password are both required
            [['kodeverivikasi'], 'required'],
            [['kodeverivikasi'], 'string', 'max' => 50],
        ];
    }
 
}

