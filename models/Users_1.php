<?php

namespace app\models;

use Yii;
use yii\base\Model;
/**
 * This is the model class for table "users".
 *
 * @property string $id
 * @property string $a_nama
 * @property string $a_alamat
 * @property string $a_email
 * @property string $a_password
 * @property string $a_no_tlp
 * @property string $a_kota_lahir
 * @property string $a_tgl_bln_thn_lahir
 * @property string $a_jkelamin
 * @property string $a_thnlulus
 * @property string $a_asalsekolah
 * @property string $a_alasanmasuk
 * @property integer $a_angkatan
 * @property string $a_nra
 * @property string $a_jabatan
 * @property string $a_spesialisasi
 * @property string $a_foto
 * @property string $a_kode
 * @property integer $level_id
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $verifyCode;
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['a_nama', 'a_email', 'a_password', 'a_no_tlp', 'a_kode', 'level_id'], 'required'],
            [['a_alamat', 'a_jkelamin', 'a_alasanmasuk', 'a_jabatan', 'a_spesialisasi'], 'string'],
            [['a_tgl_bln_thn_lahir'], 'safe'],
            [['a_angkatan', 'level_id'], 'integer'],
            [['a_nama', 'a_email', 'a_foto'], 'string', 'max' => 100],
            [['a_password', 'a_kode'], 'string', 'max' => 255],
            [['a_no_tlp'], 'string', 'max' => 17],
            [['a_kota_lahir'], 'string', 'max' => 50],
            [['a_thnlulus'], 'string', 'max' => 4],
            [['a_asalsekolah'], 'string', 'max' => 250],
            [['a_nra'], 'string', 'max' => 11],
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'a_nama' => 'A Nama',
            'a_alamat' => 'A Alamat',
            'a_email' => 'A Email',
            'a_password' => 'A Password',
            'a_no_tlp' => 'A No Tlp',
            'a_kota_lahir' => 'A Kota Lahir',
            'a_tgl_bln_thn_lahir' => 'A Tgl Bln Thn Lahir',
            'a_jkelamin' => 'A Jkelamin',
            'a_thnlulus' => 'A Thnlulus',
            'a_asalsekolah' => 'A Asalsekolah',
            'a_alasanmasuk' => 'A Alasanmasuk',
            'a_angkatan' => 'A Angkatan',
            'a_nra' => 'A Nra',
            'a_jabatan' => 'A Jabatan',
            'a_spesialisasi' => 'A Spesialisasi',
            'a_foto' => 'A Foto',
            'a_kode' => 'A Kode',
            'level_id' => 'Level ID',
            'verifyCode' => 'Verification Code',
        ];
    }
    
    //generate password md5
        public function validatePassword($password) 
        { 
                return $this->hashPassword($password,$this->a_kode)===$this->a_password; 
        } 
     
        public function hashPassword($password,$salt) 
        { 
                return md5($salt.$password); 
        } 
 
	 public function generateSalt() 
        { 
                return uniqid('',true); 
	}
}
