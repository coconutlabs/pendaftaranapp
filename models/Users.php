<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\helpers\Security;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property string $id
 * @property string $a_nama
 * @property string $a_alamat
 * @property string $a_email
 * @property string $a_password
 * @property string $a_no_tlp
 * @property string $a_kota_lahir
 * @property string $a_tgl_bln_thn_lahir
 * @property string $a_jkelamin
 * @property string $a_thnlulus
 * @property string $a_asalsekolah
 * @property string $a_alasanmasuk
 * @property integer $a_angkatan
 * @property string $a_nra
 * @property string $a_jabatan
 * @property string $a_spesialisasi
 * @property string $a_foto
 * @property string $a_kode
 * @property integer $level_id
 */
class Users extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * @inheritdoc
     */
    public $verifyCode;
    public $file;
    public $kodeverivikasi;
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['a_nama', 'a_email', 'a_password', 'a_no_tlp', 'a_kode', 'level_id'], 'required'],
            [['a_alamat', 'a_jkelamin', 'a_alasanmasuk', 'a_jabatan', 'a_spesialisasi'], 'string'],
            [['a_tgl_bln_thn_lahir'], 'safe'],
            [['a_angkatan', 'level_id'], 'integer'],
            [['file'],'file'],
            [['a_nama', 'a_email', 'a_foto'], 'string', 'max' => 100],
            [['a_password','kodeverivikasi', 'a_kode',], 'string', 'max' => 255],
            [['a_no_tlp'], 'string', 'max' => 17],
            [['a_kota_lahir'], 'string', 'max' => 50],
            [['a_thnlulus'], 'string', 'max' => 4],
            [['a_asalsekolah'], 'string', 'max' => 250],
            [['a_nra'], 'string', 'max' => 11],
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'a_nama' => 'Nama Lengkap',
            'a_alamat' => 'Alamat Rumah',
            'a_email' => 'Email',
            'a_password' => 'Password',
            'a_no_tlp' => 'No Telepon / Hp',
            'a_kota_lahir' => 'Kota Lahir',
            'a_tgl_bln_thn_lahir' => 'Tanggal Lahir',
            'a_jkelamin' => 'Jenis Kelamin',
            'a_thnlulus' => 'Tahun Lulus',
            'a_asalsekolah' => 'Nama Asal Sekolah',
            'a_alasanmasuk' => 'Alasan Masuk COCONUT',
            'a_angkatan' => 'Angkatan',
            'a_nra' => 'NRA',
            'a_jabatan' => 'Jabatan Saat Menjabat BPH',
            'a_spesialisasi' => 'Keahlian Yang Di Kuasai',
            'a_foto' => 'Foto',
            'a_kode' => 'Kode',
            'level_id' => 'Level ID',
            'verifyCode' => 'Verification Code',
            'file' => 'Foto',
        ];
    }
    
    //generate password md5
        public function validatePassword($password) 
        { 
                return $this->hashPassword($password,$this->a_kode)===$this->a_password; 
        } 
     
        public function hashPassword($password,$salt) 
        { 
                return md5($salt.$password); 
        } 
 
	 public function generateSalt() 
        { 
                return uniqid('',true); 
	}
        
            /** INCLUDE USER LOGIN VALIDATION FUNCTIONS**/
        /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
/* modified */
    public static function findIdentityByAccessToken($token, $type = null)
    {
          return static::findOne(['access_token' => $token]);
    }
 
/* removed
    public static function findIdentityByAccessToken($token)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
*/
    /**
     * Finds user by username
     *
     * @param  string    
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['a_email' => $email]);
    }

    /**
     * Finds user by password reset token
     *
     * @param  string      $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        $expire = \Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        if ($timestamp + $expire < time()) {
            // token expired
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Security::generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Security::generateRandomKey();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Security::generateRandomKey() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    /** EXTENSION MOVIE **/

}
