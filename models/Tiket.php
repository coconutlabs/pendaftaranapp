<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tiket".
 *
 * @property integer $id
 * @property integer $iduser
 * @property string $kehadiran
 */
class Tiket extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tiket';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['iduser'], 'required'],
            [['iduser'], 'integer'],
            [['kehadiran'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'iduser' => 'Iduser',
            'kehadiran' => 'Kehadiran',
        ];
    }
}
